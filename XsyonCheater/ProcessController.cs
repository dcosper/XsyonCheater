﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace XsyonCheater
{
    public class ProcessController
    {
        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess(uint dwDesiredAccess, bool bInheritHandle, int dwProcessId);
        [DllImport("kernel32.dll")]
        static extern int ReadProcessMemory(IntPtr OpenedHandle, IntPtr lpBaseAddress, byte[] lpBuffer,
                                                    uint size, out IntPtr lpNumberOfBytesRead);

        public static byte[] ReadMemory(IntPtr OpenedHandle, IntPtr BaseAddress, uint Size, ref IntPtr Bytes)
        {
            byte[] buffer = new byte[Size];
            ReadProcessMemory(OpenedHandle, BaseAddress, buffer, Size, out Bytes);
            return buffer;

        }
        public static IntPtr OpenProcess(int ProcessID)
        {
            Process MyProc = Process.GetProcessById(ProcessID);

            if (MyProc.HandleCount > 0)
            {
                IntPtr hProcess = OpenProcess((uint)ProcessAccessFlags.All, true, ProcessID); ;
                return hProcess;
            }
            else
            {
                return IntPtr.Zero;
            }

        }
        public static void RestartWithElevatedPrivliges()
        {
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.UseShellExecute = true;
            proc.WorkingDirectory = Environment.CurrentDirectory;
            proc.FileName = Assembly.GetEntryAssembly().CodeBase;

            proc.Verb = "runas";

            try
            {
                Process.Start(proc);
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("This application requires elevated credentials in order to operate correctly!");
            }
        }
        public static bool IsUserAdministrator()
        {
            bool isAdmin;
            try
            {
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException)
            {
                isAdmin = false;
            }
            catch (Exception)
            {
                isAdmin = false;
            }
            return isAdmin;
        }
        public static ProcessInfo[] GetProcessesByName(string name)
        {
            Process[] ps = Process.GetProcesses();
            List<ProcessInfo> infos = new List<ProcessInfo>();

            if (ps != null && ps.Length > 0)
            {
                for (int i = 0; i < ps.Length; i++)
                {
                    if (ps[i].ProcessName.StartsWith(name)
                        || ps[i].ProcessName.Contains(name))
                    {
                        // We dont want debugging vshost info
                        if(!ps[i].ProcessName.ToLower().Contains(".vshost"))
                        infos.Add(new ProcessInfo(ps[i].ProcessName, ps[i]));
                    }

                }
            }
            return infos.ToArray();
        }
        public static IntPtr ProcessEntryModule(IntPtr window)
        {
            return IntPtr.Zero;
        }
    }
}
