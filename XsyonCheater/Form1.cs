﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XsyonCheater
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Force run as administrator
            if (!ProcessController.IsUserAdministrator())
            {
                ProcessController.RestartWithElevatedPrivliges();
                Application.Exit();
            }
        }

        private void btn_Populate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt_Search.Text)) return;
            comboProcessess.Items.Clear();
            comboProcessess.Text = string.Empty;

            ProcessInfo[] ps = ProcessController.GetProcessesByName(txt_Search.Text);


            if (ps != null && ps.Length > 0)
            {
                for(int i = 0;i< ps.Length;i++)
                {
                    comboProcessess.Items.Add(ps[i]);
                }
                comboProcessess.SelectedIndex = 0;
            }
            else MessageBox.Show(string.Format("Unable to find any processes searching for {0}", txt_Search.Text));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // So lets send space to process
            ProcessInfo selection = comboProcessess.SelectedItem as ProcessInfo;

            string t = ProcessController.OpenProcess(selection.Process.Id).ToString("X2");

            if(selection != null)
            {
                txt_ProcessEntry.Text = t;
                User32.SetForegroundWindow(selection.Process.MainWindowHandle);
                PostMessageTools.SendSpace(selection.Process.MainWindowHandle, 0, false, false, false,
                    false, false);
            }
        }
    }
}
