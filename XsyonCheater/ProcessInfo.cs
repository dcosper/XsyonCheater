﻿using System.Diagnostics;

namespace XsyonCheater
{
    public class ProcessInfo
    {
        public string Name;
        public Process Process;

        public ProcessInfo(string name, Process process)
        {
            this.Name = name;
            this.Process = process;
        }
        public override string ToString()
        {
            return this.Name;
        }
    }
}
