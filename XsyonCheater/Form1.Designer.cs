﻿namespace XsyonCheater
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboProcessess = new System.Windows.Forms.ComboBox();
            this.txt_Search = new System.Windows.Forms.TextBox();
            this.btn_Populate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_ProcessEntry = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comboProcessess
            // 
            this.comboProcessess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboProcessess.FormattingEnabled = true;
            this.comboProcessess.Location = new System.Drawing.Point(151, 58);
            this.comboProcessess.Name = "comboProcessess";
            this.comboProcessess.Size = new System.Drawing.Size(121, 21);
            this.comboProcessess.TabIndex = 0;
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(163, 16);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.Size = new System.Drawing.Size(100, 20);
            this.txt_Search.TabIndex = 1;
            this.txt_Search.Text = "Xsyon";
            // 
            // btn_Populate
            // 
            this.btn_Populate.Location = new System.Drawing.Point(44, 58);
            this.btn_Populate.Name = "btn_Populate";
            this.btn_Populate.Size = new System.Drawing.Size(75, 23);
            this.btn_Populate.TabIndex = 2;
            this.btn_Populate.Text = "Populate";
            this.btn_Populate.UseVisualStyleBackColor = true;
            this.btn_Populate.Click += new System.EventHandler(this.btn_Populate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Processes Named Like:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(129, 167);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Send";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt_ProcessEntry
            // 
            this.txt_ProcessEntry.Location = new System.Drawing.Point(116, 228);
            this.txt_ProcessEntry.Name = "txt_ProcessEntry";
            this.txt_ProcessEntry.Size = new System.Drawing.Size(100, 20);
            this.txt_ProcessEntry.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 325);
            this.Controls.Add(this.txt_ProcessEntry);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Populate);
            this.Controls.Add(this.txt_Search);
            this.Controls.Add(this.comboProcessess);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xsyon Chater";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboProcessess;
        private System.Windows.Forms.TextBox txt_Search;
        private System.Windows.Forms.Button btn_Populate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_ProcessEntry;
    }
}

